const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const nodemailer = require("nodemailer");

const port = process.env.PORT || 8000;
const app = express();
require("dotenv").config();

// database connection
mongoose.connect(process.env.DB, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// check connection
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function () {
  console.log(`Connected to database`);
});

/*const corsOptions = {
	origin: ['http://localhost:3000'],
	optionsSuccessStatus: 200
}*/

app.use(cors());
app.options("*", cors());

// allow resource sharing from all origins
// app.use(cors());

// middlewares
app.use(express.json());

// routes
const itemRoutes = require("./routes/item");
app.use("/api/items", itemRoutes);
const categoryRoutes = require("./routes/category");
app.use("/api/categories", categoryRoutes);
const courseRoutes = require("./routes/course");
app.use("/api/courses", courseRoutes);
const userRoutes = require("./routes/user");
app.use("/api/users", userRoutes);

// email sending test

// nodemailer sender details
// const transporter = nodemailer.createTransport({
//   // SMTP - Simple Mail Transfer Protocol =  communication protocol for email transmission.
//   // host - hostname
//   // auth - defines authentication data
//   // port - the port to connect to. The default is 587. If the port is unsecured,the port numbe is 587 but from a secured host, it is 465.

//   host: "smtp.gmail.com",
//   port: 465,
//   secured: true,
//   auth: {
//     user: "qawsvill@gmail.com",
//     pass: "pangitsinessa27",
//   },
// });

// nodemailer mail details
// transporter.sendMail({
//   from: "test@example.com",
//   // ethereal email
//   to: "alicia73@ethereal.email",
//   subject: "Test Email",
//   // Message: text and html
//   // text is used if the email does not accept html format
//   text:
//     "Hello I'm QAWS VILL the developer of Next Booking System. Thank you for creating an account.",
//   html:
//     "<h1>Hello I'm QAWS VILL the developer of Next Booking System. Thank you for creating an account.</h1>",
// });

app.listen(port, () => {
  console.log(`App is listening on port ${port}`);
});
