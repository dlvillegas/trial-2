const mongoose = require("mongoose");

const categoriesSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Category name is required"],
    },
    records: [
      {
        userId: {
          type: String,
          required: [true, "User Id is required"],
        },
        items: {
          type: mongoose.Schema.ObjectId,
        },
      },
    ],
  },
  { collection: "category", timestamps: true }
);

module.exports = mongoose.model("Category", categoriesSchema);
