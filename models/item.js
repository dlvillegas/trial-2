const mongoose = require("mongoose");

const itemSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Item name is required"],
    },
    price: {
      type: Number,
      required: [true, "Item Price is required"],
    },
    category: {
      type: String,
      required: [true, "Category name is required"],
    },
    userId: {
      type: mongoose.Schema.ObjectId,
      required: [true, "User id is required"],
    },
  },
  { collection: "item", timestamps: true }
);

module.exports = mongoose.model("Item", itemSchema);
