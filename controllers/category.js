const Category = require("./../models/category");

module.exports.get = (params) =>
  Category.findById(params).then((category) => category);

module.exports.getAll = function (params) {
  // return all categories
  // 	categories result of our query
  return Category.find(params).then((categories) => categories);
};

module.exports.add = function (params) {
  let category = new Category({
    name: params.name,
  });

  return category.save().then((category, err) => {
    if (err) {
      return false;
    } else {
      return category;
    }
  });
};

module.exports.update = (params) => {
  let { categoryId, name } = params;
  return Category.findByIdAndUpdate(categoryId, {
    name,
  }).then((doc, err) => {
    return err ? false : true;
  });
};
