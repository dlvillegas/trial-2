const Item = require("./../models/item");
const Category = require("./../models/category");
const User = require("./../models/user");
const mongoose = require("mongoose");

module.exports.get = (params) => Item.findById(params).then((item) => item);

module.exports.getAll = function (params) {
  // return all items
  // 	items result of our query
  return Item.find(params).then((items) => items);
};

module.exports.add = async function (params) {
  try {
    const category = await Category.findOne({
      name: params.category,
    });

    if (!category) {
      return "Category doesn't exist";
    }

    const user = await User.findById({
      _id: mongoose.Types.ObjectId(params.userId),
    });

    if (!user) {
      return "User id doesn't exist";
    }

    let item = new Item({
      name: params.name,
      price: params.price,
      category: params.category,
      userId: params.userId,
    });

    category.records.push({ userId: params.userId, item });
    await category.save();

    if (category.name === "Income") {
      user.wallet += item.price;
    }
    if (category.name === "Expense") {
      user.wallet -= item.price;
    }
    await user.save();

    return await item.save();
  } catch (err) {
    return err;
  }
};

module.exports.update = (params) => {
  let { itemId, name } = params;
  return Item.findByIdAndUpdate(itemId, {
    name,
  }).then((doc, err) => {
    return err ? false : true;
  });
};
