const Course = require('./../models/course');

module.exports.add = function(params) {

	let course = new Course({
		name: params.name,
		description: params.description,
		price: params.price,
	})

	return course.save().then(( course, err) => {
		if (err) {
			return false
		} else{
			return true
		}
	})
}

module.exports.getAll = function() {
	// return all courses
	// 	courses result of our query
	return Course.find({ isActive: true}).then( courses => courses)
}

module.exports.get = (params) => Course.findById(params).then(course => course)

module.exports.update = params => {
	let { courseId, name, description, price } = params
	return Course.findByIdAndUpdate(courseId, { name, description, price}).then( (doc, err) => {
		return err ? false : true
	} )
}

module.exports.archive = params => {

	return Course.findByIdAndUpdate(params, {isActive: false})
	.then( (doc, err )=> {
		return err ? false : true
	})
}

module.exports.getAllCourses = () => {

	return Course.find({}).then(resultFromFind => resultFromFind)

}

module.exports.activate = (params) => {
	
	console.log(params)

	let updateActive = {

		isActive: true

	}

	return Course.findByIdAndUpdate(params.id,updateActive).then((course, err)=>{

		return (err) ? false : true

	})

}