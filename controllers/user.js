const User = require("../models/user");
const Course = require("./../models/course");
const bcrypt = require("bcrypt");
const { createAccessToken } = require("./../auth");
const nodemailer = require("nodemailer");

// this package allows the use of google Login client and verify the tokenId
const { OAuth2Client } = require("google-auth-library");
// save the clientId for our google login:
const clientId =
  "1013780524186-u2455hji1fnhdllscsa1sja67gp312t3.apps.googleusercontent.com";
// Note: clientId used in the google Login button must match the clientId here in the backend.

module.exports.emailExists = (params) => {
  return User.find({ email: params.email }).then((result) => {
    return result.length > 0 ? true : false;
  });
};

module.exports.register = (params) => {
  let newUser = new User({
    firstName: params.firstName,
    lastName: params.lastName,
    email: params.email,
    mobileNo: params.mobileNo,
    // 10 = salt/string of characters added to the password before hashing
    // "juan1234abcdefghij" = "fh2lkj3hlj2h3t"
    password: bcrypt.hashSync(params.password, 10),
    loginType: "email",
  });

  return newUser.save().then((user, err) => {
    return err ? false : true;
  });
};

module.exports.login = (req, res) => {
  // email is correct but the password is not correct
  // email and password are not correct
  // email and password are correct
  // email is correct but password is not correct
  User.findOne({ email: req.body.email })
    .then((user) => {
      if (!user) {
        res.send(false);
      } else {
        // res.send(user)
        let comparePasswordResult = bcrypt.compareSync(
          req.body.password,
          user.password
        );
        if (!comparePasswordResult) {
          res.send(false);
        } else {
          res.send({ accessToken: createAccessToken(user) });
        }
      }
    })
    .catch((err) => {
      res.status(500).send("Server Error");
    });
};

module.exports.get = (params) => {
  return User.findById(params.userId)
    .select({ password: 0 })
    .then((user) => {
      return user;
    });
};

module.exports.enroll = (params) => {
  return User.findById(params.userId).then((user) => {
    user.enrollments.push({ courseId: params.courseId });
    return user.save().then((user) => {
      return Course.findById(params.courseId).then((course) => {
        course.enrollees.push({ userId: params.userId });
        return course.save().then((course) => {
          return course ? true : false;
        });
      });
    });
  });
};

module.exports.getAllUsers = () => {
  return User.find({}).then((resultFromFind) => resultFromFind);
};

module.exports.sample = () => {
  let dataSample = {
    response: "hello, this is from the server.",
  };

  let data = new Promise((res, rej) => {
    res(dataSample);
  });

  return data;
};

// Google Login
module.exports.verifyGoogleTokenId = async (tokenId, googleAccessToken) => {
  // console.log(tokenId);
  console.log(googleAccessToken);

  // create a new OAuth2Client object with our clientId for identification and authorization to use OAuth2
  const client = new OAuth2Client(clientId);
  const data = await client.verifyIdToken({
    idToken: tokenId,
    audience: clientId,
  });
  // audience and idToken are required to check your users Google Login Token and your client id side by side to check if it is legitimate and is coming from a recognizable source

  // Let's check if we can reeceive verified data:
  // console.log(data);

  // Run this if the email from the data payload is verified
  if (data.payload.email_verified === true) {
    // We're going to check if the user's email has already been registered in our database.
    // .exec() in mongoose, works like you .then() and that it allows the execution of the following statements.
    const user = await User.findOne({ email: data.payload.email }).exec();
    console.log(user);
    // if the user variable logs null, then the user has not registered in our db yet.
    // if the user variable logs details then the user has already registered.
    if (user !== null) {
      console.log("A user with the same email has been registered.");
      console.log(user);
      // This will run if the user variable does not log null, and instead contains the user details. Which means, a user with the same email has been registered. However, we will now check if the registered user used google login to register or used our regular registration to register.

      // If the email has already been used by registering through our regular registration we will send an error message.
      if (user.loginType === "google") {
        return { accessToken: createAccessToken(user) };
      } else {
        return { error: "login-type-error" };
      }
    } else {
      // this will run if the google login user logged onto our web app for the first time.
      // we will register his/her details into our database.
      console.log(data.payload);
      const newUser = new User({
        firstName: data.payload.given_name,
        lastName: data.payload.family_name,
        email: data.payload.email,
        loginType: "google",
      });

      // save our new user into the db:
      return newUser.save().then((user, err) => {
        // Email Sending Feature:
        // Send an email to our user thanking them for registering to our Next Booking System.

        // mailOptions =  will be the content of the email we are going to send to our user. This will include the messagem email address of the sender, and email address of the recipient.
        const mailOptions = {
          from: "dlvillegas@up.edu.ph",
          to: user.email,
          subject: "Thank you for registering to Next Booking System.",
          text: `You registered to Next Booking System on ${new Date().toLocaleString()}`,
          html: `You registered to Next Booking System on ${new Date().toLocaleString()}`,
        };

        // We are going to pass details to our nodemailer by sending authorizations and tokens so that we don't have to use or hard code our email sender credentials.

        const transporter = nodemailer.createTransport({
          host: "smtp.gmail.com",
          port: 465,
          secure: true,
          auth: {
            type: "OAuth2",
            user: process.env.MAILING_SERVICE_EMAIL,
            clientId: process.env.MAILING_SERVICE_CLIENT_ID,
            clientSecret: process.env.MAILING_SERVICE_CLIENT_SECRET,
            refreshToken: process.env.MAILING_SERVICE_REFRESH_TOKEN,
            accessToken: googleAccessToken,
          },
        });

        // create a function to send our mail:
        // use transporter as the params to send our email with the proper authorizations:
        // check if theresult of the email sending.
        function sendMail(transporter) {
          transporter.sendMail(mailOptions, function (err, result) {
            if (err) {
              console.log(err);
              transporter.close();
            } else if (result) {
              console.log(result);
              transporter.close();
            }
          });
        }

        // use the sendMail function to send an email to the user:
        // with the transporter as an argument to the sendMail function.
        sendMail(transporter);

        // create our new user an accesstoken to immediately login
        return { accessToken: createAccessToken(user) };
      });
    }
  } else {
    // this will run if somehow, the google tokenId given has an error or is more than legitimate
    return { error: "google-auth-error" };
  }
};
