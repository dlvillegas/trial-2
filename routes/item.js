const router = require("express").Router();
const Item = require("./../models/item");
const ItemController = require("./../controllers/item");
const auth = require("../auth");

// get all items
router.get("/", auth.verify, (req, res) => {
  ItemController.getAll(req.body).then((items) => res.send(items));
});

// get item by id
router.get("/:id", auth.verify, (req, res) => {
  ItemController.get(req.params.id).then((result) => res.send(result));
});

// add item
router.post("/", auth.verify, (req, res) => {
  ItemController.add(req.body).then((result) => res.send(result));
});

// update item
router.put("/", auth.verify, (req, res) => {
  ItemController.update(req.body).then((result) => res.send(result));
});

// delete item
router.delete("/:id", auth.verify, (req, res) => {
  ItemController.archive(req.params.id).then((result) => res.send(result));
});

module.exports = router;
