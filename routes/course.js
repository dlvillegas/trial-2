const router = require('express').Router()
const Course = require('./../models/course');
const CourseController = require('./../controllers/course');
const auth = require('./../auth');

// get all courses
router.get('/', (req,res)=> {
	CourseController.getAll().then(courses => res.send(courses));
});

router.get('/all', (req,res)=>{

	CourseController.getAllCourses().then(resultFromGetAllCourses => res.send(resultFromGetAllCourses))

})

// get course by id
router.get('/:id', (req,res)=> {
	CourseController.get(req.params.id).then( result => res.send(result))
});


// add course
router.post('/', auth.verify, (req,res)=> {
	CourseController.add(req.body).then( result => res.send(result))
});

// update course
router.put('/', auth.verify, (req,res) => {
	CourseController.update(req.body).then( result => res.send(result))
});

router.put('/activate/:id', auth.verify, (req,res)=>{

	CourseController.activate(req.params).then(resultFromActivate => res.send(resultFromActivate))

})


// delete course
router.delete('/:id', auth.verify, (req,res) => {
	CourseController.archive(req.params.id).then( result => res.send(result))
})

module.exports = router;