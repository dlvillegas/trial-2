const router = require("express").Router();
const Category = require("./../models/category");
const CategoryController = require("./../controllers/category");
const auth = require("../auth");

// get all categories
router.get("/", auth.verify, (req, res) => {
  CategoryController.getAll(req.body).then((categories) =>
    res.send(categories)
  );
});

// get category by id
router.get("/:id", auth.verify, (req, res) => {
  CategoryController.get(req.params.id).then((result) => res.send(result));
});

// add category
router.post("/", auth.verify, (req, res) => {
  CategoryController.add(req.body).then((result) => res.send(result));
});

// update category
router.put("/", auth.verify, (req, res) => {
  CategoryController.update(req.body).then((result) => res.send(result));
});

// delete category
router.delete("/:id", auth.verify, (req, res) => {
  CategoryController.archive(req.params.id).then((result) => res.send(result));
});

module.exports = router;
